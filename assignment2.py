from flask import Flask, render_template, request, redirect, url_for, jsonify, json
import random
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON')
def bookJSON():
    r=json.dumps(books)
    return r

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html',books=books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method=='POST':
        newkey=request.form['name']
        if newkey=="":
            return render_template('newBook.html')
        else:
            dict={'title':newkey, 'id':len(books)+1}
            books.append(dict)
            return redirect(url_for('showBook'))
    else:
        return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method=='POST':      
        newkey=request.form['name']
        if newkey=="":
            return render_template('editbook.html', book_id=book_id)
        else:
            books[book_id-1]['title']=newkey
            return redirect(url_for('showBook'))
    else:
        return render_template('editBook.html',book_id=book_id)
        
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method=='POST':
        del books[book_id-1]
        return redirect(url_for('showBook'))
    else:
        return render_template('deleteBook.html', book_id=book_id, books=books)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

